import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TemporaryDrawer from './components/TemporaryDrawer'
import PaginationComp from './components/PaginationComp'
import Data from './data/information'
import './css/app.css'
const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numbers: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      value: '',
      currentPage: 1,
    };
  }

  toggleDrawer = (side, open, number) => () => {
    this.setState({
      [side]: open,
      value: number,
    });
  };

  handlePageChange = page => {
    this.setState({
      currentPage: page
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div >
        <TemporaryDrawer
          open={this.state.right}
          onClose={this.toggleDrawer('right', false)}
          value={this.state.value}
        />
        <div className="App">
          <Grid container spacing={24} justify="center">
            {Data.map((data, i) =>
              <Grid item xs={3} key={"card" + i}>
                <Card className={classes.name}>
                  <CardContent>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                      Word of the Day {data.color}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small" onClick={this.toggleDrawer('right', true, data.value)}>Learn More</Button>
                  </CardActions>
                </Card>
              </Grid>)}
           <PaginationComp />
          </Grid>

        </div>

      </div>

    );
  }

}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);

// export default App;