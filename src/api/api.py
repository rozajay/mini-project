# http://127.0.0.1:5000/?per_page=12&page=4
from flask import Flask, render_template, jsonify
from flask_paginate import Pagination, get_page_args


app = Flask(__name__)
app.template_folder = ''
items = [
    {
        "color": "red1",
        "value": "#f00"
    },
    {
        "color": "green1",
        "value": "#0f0"
    },
    {
        "color": "blue1",
        "value": "#00f"
    },
    {
        "color": "cyan1",
        "value": "#0ff"
    },
    {
        "color": "magenta1",
        "value": "#f0f"
    },
    {
        "color": "yellow1",
        "value": "#ff0"
    },
    {
        "color": "black1",
        "value": "#000"
    },
    {
        "color": "black1",
        "value": "#000"
    },
    {
        "color": "black1",
        "value": "#000"
    },
    {
        "color": "black1",
        "value": "#000"
    },
    {
        "color": "black1",
        "value": "#000"
    },
    {
        "color": "black1",
        "value": "#000"
    }
]

def get_items(offset=0, per_page=10):
    return items[offset: offset + per_page]

@app.route('/')
def index():
    page, per_page, offset = get_page_args(page_parameter='page',
                                           per_page_parameter='per_page')
    pagination_tasks = get_items(offset=offset, per_page=per_page)

    return jsonify(items=pagination_tasks)

if __name__ == '__main__':
    app.run(debug=True)
