import React from 'react';
import Pagination from 'react-paginating';
import Button from '@material-ui/core/Button';
import Data from '../data/information'
// const fruits = [
//   ['apple', 'orange'],
//   ['banana', 'avocado'],
//   ['coconut', 'blueberry'],
//   ['payaya', 'peach'],
//   ['pear', 'plum'],
//   ['apple', 'orange'],
//   ['banana', 'avocado'],
//   ['coconut', 'blueberry'],
//   ['payaya', 'peach'],
//   ['pear', 'plum']
// ];
const fruits = Data;
const limit = 2;
const pageCount = 3;
const total = fruits.length * limit;

class PaginationComp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1
    };
    this.handleNextPageChange = this.handleNextPageChange.bind(this);
    this.handlePreviousPageChange = this.handlePreviousPageChange.bind(this);
  }

  handlePreviousPageChange = (pageStatus, total, page) => {
    if (pageStatus === true && page !== 0){
      this.setState({
        currentPage: page
      });
    } else {
      this.setState({
        currentPage: total
      });
    }
  };

  handleNextPageChange = (pageStatus, total, page) => {
    if (pageStatus === true && page !== total+1){
      this.setState({
        currentPage: page
      });
    } else {
      this.setState({
        currentPage: 1
      });
    }

  };

  render() {
    const { currentPage } = this.state;
    return (
      <div>
        <ul>
          {/* {fruits[currentPage - 1].map(item => <li key={item}>{item}</li>)} */}
          {console.log(fruits[currentPage-1].color)}
          {fruits[currentPage-1].color}
        </ul>
        <Pagination
          total={total}
          limit={limit}
          pageCount={pageCount}
          currentPage={currentPage}
        >
          {({
            pages,
            currentPage,
            hasNextPage,
            hasPreviousPage,
            previousPage,
            nextPage,
            totalPages,
            getPageItemProps
          }) => (
              <div>
                <Button
                  {...getPageItemProps({
                    pageValue: previousPage,
                    onPageChange: (e) => this.handlePreviousPageChange(hasPreviousPage, totalPages, e)
                  })}
                >
                  {'Back'}
                </Button>
                {'  Page ' + currentPage + ' of ' + totalPages + '  '}
                <Button
                  {...getPageItemProps({
                    pageValue: nextPage,
                    onPageChange: (e) => this.handleNextPageChange(hasNextPage, totalPages, e)
                  })}
                >
                  {'Next'}
                </Button>
              </div>
            )}
        </Pagination>
      </div >
    );
  }
}

export default PaginationComp;